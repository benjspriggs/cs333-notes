# CS 333 - Introduction to Operating Systems
Learning basic concepts for operating systems,
using an implementation of Unix with ANSI C.
Taken in the Winter quarter 2017.

These files comprise utility scripts and other notes
prepared over the course of taking this class.

[Course website](http://web.cecs.pdx.edu/~markem/CS333/)

## Syllabus
<http://web.cecs.pdx.edu/~markem/CS333/syllabi/2017W>
## Course Schedule
All projects due 11:59 PM PST on the date listed.

| Project | Date Due | Website                                                        |
|---------+----------+----------------------------------------------------------------|
| 1       | 1/15     | [Project 1](http://web.cecs.pdx.edu/~markem/CS333/projects/p1) |
| 2       | 1/29     | [Project 2](http://web.cecs.pdx.edu/~markem/CS333/projects/p2) |
| 3       | 2/12     | [Project 3](http://web.cecs.pdx.edu/~markem/CS333/projects/p3) |
| 4       | 2/26     | [Project 4](http://web.cecs.pdx.edu/~markem/CS333/projects/p4) |
| 5       | 3/12     | [Project 5](http://web.cecs.pdx.edu/~markem/CS333/projects/p5) |

## Fun Facts

To get to the Qemu CLI (and quit out of the virtual box) use `C-a C`.
