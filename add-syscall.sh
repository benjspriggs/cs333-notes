#!/bin/bash
# Answers the question:
# What do I do to add a new syscall in xv6?
# TODO: find if there's a way to
# more automatically add syscalls

echo "Q: How do I add a new syscall in xv6?"
echo
echo "A: Edit a few files:"

files=(
"user.h"
"usys.S"
"syscall.h"
"syscall.c"
"syscall.c"
"sysproc.c"
"call.c"
"Makefile"
)
edit=(
"Add the function prototype"
"Add SYSCALL(call)"
"Add line mapping number to sys_call (FOLLOW CONVENTION!)"
"Add entry point (extern int call(void))"
"Add line in syscalls array"
"Implement sys_call"
"Implement system program (if necessary)"
"Add program to UPROGS (if program is added)"
)

for i in ${!files[*]};
do
  printf "%10s| %s\n" "${files[i]}" "${edit[i]}"
done
