#!/usr/bin/env bash
# get all of the files changed in git
# since the last assignment
# REQUIRED: tags for each assignment state
# written by :: Benjamin Spriggs

# source common error definition
source $(dirname $(realpath $0))/err.sh

_usage="./changed.sh <assignment-number> <dist-repo> "

[[ -z "$1" ]] && err "Missing assignment argument"
[[ -z "$2" ]] && err "Missing distribution repo"

assignment="$1"
repo="$2"

# validate if a tags exists in a the dist repo
validate_exists() {
  for (( i=1; i<=$#; i++)); do
    if ! GIT_DIR=./"$repo"/.git git rev-parse ${!i} >/dev/null 2>&1
    then
      err "Tag ${!i} not found in '$repo'."
    fi
  done
}

# files since last commit:
#  git ls-tree --name-only
# files between two commits:
#  git diff --name-only <commit-ish> <commit-ish>

between_diff() {
  validate_exists "$repo" "$assignment"
  pushd . >&2
  cd "$repo"
  git diff --name-only "$1" "$2"
  popd >&2
}

case "$assignment" in
  a1) between_diff init "$assignment"
    ;;
  a[2-4]) between_diff a$((${1##[^0-9]} - 1)) "$assignment"
    ;;
  *) 
    >&2 echo "Not a recognized assignment ('$assignment' is not in a[1-4])"
    ;;
esac
