#!/bin/bash
# Download, unpack, and setup
# the PSU variant of the xv6 OS
# defaulting to the current directory,
# into a folder named 'xv6-psu-kernel'
# usage:
#   ./install [<path-to-install>]

# location of the xv6 kernel on the web
xv6_kernel_web="http://web.cecs.pdx.edu/~markem/CS333/xv6/xv6-pdx-kernel.tar.gz"
tar="$PWD/xv6-psu-kernel.tar"
_loc="$(realpath -q $1)"
install_location="${_loc:-$PWD/xv6-psu-kernel}"
install_location="$(realpath $install_location)"

# download the kernel
echo "Downloading the kernel..."
wget -O "$tar" "$xv6_kernel_web" \
&& mkdir -p "$install_location" \
&& tar xf "$tar" -C "$install_location" \
&& echo "xv6 kernel downloaded and unpacked successfully."

# find the makefile and go there
safe_path="$(find "$install_location" -name "Makefile" -exec dirname {} \;)"
safe_path="$(realpath $safe_path)"

# delete tar file
rm "$tar"

# backup old gdbinit
if [[ -e ~/.gdbinit ]]; then 
  echo "Backing up gdbinit..." 
  cp ~/.gdbinit ~/.gdbinit.bak
fi

# add auto-load safe path to gdbinit
echo "add-auto-load-safe-path $safe_path/.gdbinit" \
  >> ~/.gdbinit

echo "Added auto-load-safe-path to gdbinit successfully."
echo "Finished!"
echo
