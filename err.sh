#!/usr/bin/env bash
# function for an error

err() {
  (>&2 echo "Error: $@"; 
  echo "$_usage")
  exit 1
}

